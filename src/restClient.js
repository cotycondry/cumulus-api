var Client = require('node-rest-client').Client
var config = require('./config')

module.exports = new Client({
  user: config.finesse.user,
  password: config.finesse.pass
})
