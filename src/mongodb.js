var MongoClient = require('mongodb').MongoClient
// var assert = require('assert')
var ObjectId = require('mongodb').ObjectID
var config = require('./config')
var url = `mongodb://${config.db.host}:${config.db.port}/flm`

var insertDocument = function(db, collection, data, callback) {
  db.collection(collection).insertOne(data, function(err, result) {
    // assert.equal(err, null)
    if (err) throw err
    console.log(`mongodb: inserted a document into the ${collection} collection`)
    callback()
  })
}

function retrieve (collection, callback) {
  if (config.db.enabled) {
    MongoClient.connect(url, function(err, db) {
      db.collection(collection).find().toArray(function (err, result) {
        if (err) throw err
        console.log(`mongodb retrieved ${result.length} records from ${collection} collection`)
        callback(result)
      })
    })
  } else {
    callback('Database not enabled.')
  }
}

function findOne (collection, query, callback) {
  console.log('query =', query)
  if (config.db.enabled) {
    MongoClient.connect(url, function(err, db) {
      db.collection(collection).findOne(query, function (err, result) {
        if (err) throw err
        console.log(`mongodb retrieved record from ${collection} collection`)
        // console.log('result =', result)
        callback(result)
      })
    })
  } else {
    callback('Database not enabled.')
  }
}

function insert (collection, data) {
  if (config.db.enabled) {
    MongoClient.connect(url, function(err, db) {
      if (err) throw err
      insertDocument(db, collection, data, function() {
        db.close()
      })
    })
  } else {
    // do nothing
  }
}

module.exports = {
  insert,
  get: retrieve,
  logRequest (req) {
    insert('logs', {
      headers: req.headers,
      url: req.url,
      method: req.method,
      body: req.body,
      baseUrl: req.baseUrl,
      originalUrl: req.originalUrl,
      params: req.params,
      query: req.query
    })
  },
  logResponse (res) {
    insert('logs', res, () => {
      db.close()
    })
  },
  addAuthToken (user, token, seconds) {
    insert('tokens', {
      user,
      token,
      expiry: seconds + Math.round(new Date().getTime() / 1000)
    })
  },
  getUser ({token}) {
    return new Promise((resolve, reject) => {
      findOne('tokens', {token}, (result, err) => {
        console.log('err =', err)
        console.log('result =', result)
        if (err) reject(err)
        else resolve(result)
      })
    })
  }
}
