var express = require('express')
var router = express.Router()
var team = require('src/models/cce/team')
var supervisor = require('src/models/cce/supervisor')
var db = require('src/mongodb')
// var xml2js = require('xml2js')

// get team list
router.get('/', function (req, res, next){
  // log
  // db.logRequest(req)
  try {
    team.list(function (data) {
      // log
      // db.logResponse(data)
      var prettyJson = JSON.stringify(data, null, 2)
      // return pretty JSON
      res
      .set("Content-type", "application/json; charset=utf-8")
      .send(prettyJson)
    })
  } catch (e) {
    var prettyError = JSON.stringify(e, null, 2)
    res
    .set("Content-type", "application/json; charset=utf-8")
    .send(prettyError)
  }
})

// get teams list for specified username
router.get('/:user', function (req, res, next){
  console.log('getting teams for user', req.params.user)
  // log to db
  db.logRequest(req)
  supervisor.getTeams(req.params.user, function (data) {
    var prettyJson = JSON.stringify(data, null, 2)
    // return pretty JSON
    res
    .set("Content-type", "application/json; charset=utf-8")
    .send(prettyJson)
  })
})

module.exports = router
