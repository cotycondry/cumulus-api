var config = require('src/config.js')
var unirest = require('unirest')
var parseString = require('xml2js').parseString

var urlBase = `https://${config.cce.host}/unifiedconfig/config/agentteam`

module.exports = {
  list: () => {
    return new Promise((resolve, reject) => {
      //TODO add support for more than 100 results
      unirest.get(urlBase)
      .auth({
        user: config.cce.user,
        pass: config.cce.pass
      })
      .query({
        // summary: true,
        resultsPerPage: 100
      })
      .end(function (response) {
        if (response.status >= 200 && response.status < 300) {
          // parse XML to JSON
          parseString(response.body, {explicitArray: false}, function (err, result) {
            if (err) throw err
            else try {
              let teams = result.results.agentTeams.agentTeam
              for (let team of teams) {
                team.id = team.refURL.substring(team.refURL.lastIndexOf('/') + 1)
              }
              resolve(teams)
            } catch (e) {
              reject(e)
            }
          })
        } else {
          console.log('error - ', response.body)
          if(response) {
            reject(response)
          } else {
            resolve('no response returned from CCE get team list API')
          }
        }
      })
    })
  },
  get: function (id, callback) {
    var url = `${urlBase}/${id}`
    unirest.get(url)
    .end(function (response) {
      // console.log(response.body)
      callback(response.body)
    })
  }
}
