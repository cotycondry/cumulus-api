var express = require('express')
var router = express.Router()
var team = require('../models/finesse/team')
var db = require('../mongodb')
// var user = require('src/models/user')
var ccesupervisor = require('src/models/cce/supervisor')

// get team list
router.get('/', function (req, res, next){
  // log
  // db.logRequest(req)

  // get and log X-Auth-Token
  var authToken = req.header('X-Auth-Token')
  console.log('auth token: ', authToken)
  // get user by auth token
  db.getUser({token: authToken})
  .then(data => {
    if (data.user === 'administrator') {
      // return all teams list
      team.list()
      .then(data2 => {
        res
        .set("Content-type", "application/json; charset=utf-8")
        .send(JSON.stringify(data2, null, 2));
      }).catch(err => {
        console.log('server error response:', err.status + ' - ' + err.body)
        res.status(500).send(err.body)
      })
    } else {
      // supervisor user? list supervised teams from CCE api
      ccesupervisor.getTeams(data.user)
      .then(data3 => {
        var prettyJson = JSON.stringify(data3, null, 2)
        // return pretty JSON
        res
        .set("Content-type", "application/json; charset=utf-8")
        .send(prettyJson)
      })
    }
  })
  .catch(response => {
    try {
      console.log('server error response:', response.status + ' - ' + response.body)
      res.status(500).send(response.body)
    } catch (e) {
      console.log('server error: ', response)
      res.status(500).send(response)
    }
  })


})

// get single team details
router.get('/:id', function (req, res, next){
  // log
  // db.logRequest(req)
  // get and log X-Auth-Token
  var authToken = req.header('X-Auth-Token')
  console.log('auth token: ', authToken)
  console.log('getting Finesse Team ' + req.params.id + ' details')
  team.get(req.params.id, function (data) {
    // log
    // db.logResponse(data)
    res.json(data)
  })
})

// get single team layout config
router.get('/:id/layout', function (req, res, next){
  // log
  // db.logRequest(req)
  // get and log X-Auth-Token
  var authToken = req.header('X-Auth-Token')
  console.log('auth token: ', authToken)
  console.log('getting Finesse Team ' + req.params.id + ' Layout Config')
  team.getLayout(req.params.id, function (data) {
    // log
    // db.logResponse(data)
    res.json(data);
  })
})

// set team's layout config
router.put('/:id/layout', function (req, res, next) {
  // log
  // db.logRequest(req)
  // get and log X-Auth-Token
  var authToken = req.header('X-Auth-Token')
  console.log('auth token: ', authToken)
  team.updateLayout(req.params.id, req.body, function (err, data) {
    if (err) {
      // console.log('error: ', err)
      // display error
      res.status(500).send(err)
    } else {
      // return OK
      res.sendStatus(200)
    }
  })
})

module.exports = router
