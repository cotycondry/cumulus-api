var config = require('src/config.js')
var client = require('src/restClient.js')
var unirest = require('unirest')
var js2xmlparser = require('js2xmlparser')
var parseString = require('xml2js').parseString

var urlBase = `https://${config.finesse.host}/finesse/api/Team`

module.exports = {
  list: () => {
    return new Promise((resolve, reject) => {
      var url = `${urlBase}s`
      unirest.get(url)
      .auth({
        user: config.finesse.user,
        pass: config.finesse.pass
      })
      .end(function (response) {
        console.log('List Finesse Teams - response status = ' + response.statusCode)
        if (response.statusCode >= 200 && response.statusCode < 300) {
          // parse XML to JSON
          parseString(response.body, {explicitArray: false}, function (err, result) {
            // console.log(result)
            if (err) reject(err)
            else resolve(result.Teams.Team)
          })
        } else reject(response)
      })
    })
  },
  get: function (id, callback) {
    var url = `${urlBase}/${id}`
    client.get(url, function (data, rsp) {
      callback(data)
    })
  },
  getLayout: function (id, callback) {
    var url = `${urlBase}/${id}/LayoutConfig`
    client.get(url, function (data, rsp) {
      callback(data)
    })
  },
  updateLayout: function (id, data, callback) {
    var url = `${urlBase}/${id}/LayoutConfig`
    var xmlData = js2xmlparser.parse('TeamLayoutConfig', data.TeamLayoutConfig)

    unirest
    .put(url)
    .headers({
      'Accept': 'application/xml',
      'Content-Type': 'application/xml'
    })
    .auth({
      user: config.finesse.user,
      pass: config.finesse.pass
    })
    .send(xmlData)
    .end(function (response) {
      console.log('Update Finesse team layout - response status = ' + response.statusCode)
      if (response.statusCode >= 200 && response.statusCode < 300) {
        callback(false, response)
      } else {
        callback(response, false)
      }
    })
  }
}
