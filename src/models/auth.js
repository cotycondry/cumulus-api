var db = require('src/mongodb')
var hat = require('hat')

module.exports = {
  authenticate ({user, pass, success, fail}) {
    //TODO add real authentication
    // create token

    var token = hat()
    // add auth token to the database for this user, for 24 hours
    db.addAuthToken(user, token, 24 * 60 * 60)
    // return the token
    success(token)
  },
  getUser: db.getUser
}
