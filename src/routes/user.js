var express = require('express')
var router = express.Router()
var db = require('../mongodb')

router.get('/', function (req, res, next){
  // get and log X-Auth-Token
  var authToken = req.header('X-Auth-Token')

  db.getUser(authToken)
  .then(data => {
    console.log(data)
    res
    .set("Content-type", "application/json; charset=utf-8")
    .send(JSON.stringify({user: data.user}, null, 2));
  }).catch(err => {
    console.log('db error:', err)
    res.json(err)
  })
})

module.exports = router
