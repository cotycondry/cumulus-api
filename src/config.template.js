module.exports = {
  system: {
    port: '3001'
  },
  finesse: {
    host: 'finesse1.dcloud.cisco.com',
    user: 'admin',
    pass: 'C1sco12345'
  },
  cce: {
    host: 'ccedata.dcloud.cisco.com',
    user: 'administrator@dcloud.cisco.com',
    pass: 'C1sco12345'
  },
  db: {
    enabled: true,
    host: '127.0.0.1',
    port: '27017'
  },
  email: {
    // host: 'mail1.dcloud.cisco.com',
    host: 'mail1.dcloud.cisco.com',
    port: 25,
    secure: false // secure:true for port 465, secure:false for port 587
    // auth: {
    //   user: 'username@example.com',
    //   pass: 'userpass'
    // }
  }
}
