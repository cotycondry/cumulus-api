var config = require('src/config.js')
var unirest = require('unirest')
var cceteam = require('src/models/cce/team')

// var urlBase = `https://${config.finesse.host}/finesse/api/Team`

module.exports = {
  // retrieve supervised teams list for provided supervisor username
  getTeams: supervisor => {
    return new Promise((resolve, reject) => {
      // get full teams list
      cceteam.list()
      .then(data => {
        // console.log('data = ', data)
        // filter teams using the specified supervisor username
        var filteredData = data.filter(v => {
          // console.log('v = ', v)
          // does this element have any supervisors?
          if (v.supervisors && v.supervisors.supervisor) {
            let list = v.supervisors.supervisor
            if (!Array.isArray(list)) {
              return list.userName === supervisor
            } else {
              // include this element if the specified supervisor is in the supervisors list
              return list.find(x => {
                // console.log('userName =', x.userName[0])
                return x.userName === supervisor
              }) !== undefined
            }
          } else {
            // no supervisors on this team, so we can't be one of them
            return false
          }
        })
        resolve(filteredData)
      })
      .catch(e => {
        reject(e)
      })
    })
  }
}
