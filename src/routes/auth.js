var express = require('express')
var router = express.Router()
var auth = require('src/models/auth')

router.post('/login', function (req, res, next){
  console.log('req.body =', req.body)
  console.log('user ' + req.body.user + ' attempting authentication')
  try {
    auth.authenticate({
      user: req.body.user,
      pass: req.body.pass,
      success: data => {
        res
        .set("Content-type", "application/json; charset=utf-8")
        .send(JSON.stringify({token: data}, null, 2));
      },
      fail: data => {
        console.log('error:', data)
        res.json(data)
      }
    })
  } catch (e) {
    console.log('error:', e)
    res.json(e)
  }
})

module.exports = router
